/****************************************************************************************
 * Life would be much simpler if we could just add mk_varlike to backend/utils/adt/like.c
 * rather than having to redefine all the static helper functions here too
 ***************************************************************************************/

#include "postgres.h"

#include <ctype.h>

#include "catalog/pg_collation.h"
#include "mb/pg_wchar.h"
#include "miscadmin.h"
#include "utils/builtins.h"
#include "utils/pg_locale.h"


#define LIKE_TRUE                                               1
#define LIKE_FALSE                                              0
#define LIKE_ABORT                                              (-1)


static int SB_MatchText(char *t, int tlen, char *p, int plen,
                         pg_locale_t locale, bool locale_is_c);
//static text *SB_do_like_escape(text *, text *);

static int MB_MatchText(char *t, int tlen, char *p, int plen,
                         pg_locale_t locale, bool locale_is_c);
//static text *MB_do_like_escape(text *, text *);

static int UTF8_MatchText(char *t, int tlen, char *p, int plen,
                           pg_locale_t locale, bool locale_is_c);

static int SB_IMatchText(char *t, int tlen, char *p, int plen,
                          pg_locale_t locale, bool locale_is_c);

static int      GenericMatchText(char *s, int slen, char *p, int plen);
static int      Generic_Text_IC_like(text *str, text *pat, Oid collation);


/*--------------------
 * Support routine for MatchText. Compares given multibyte streams
 * as wide characters. If they match, returns 1 otherwise returns 0.
 * --------------------
 */
static inline int
wchareq(char *p1, char *p2)
{
        int                     p1_len;

        /* Optimization:  quickly compare the first byte. */
        if (*p1 != *p2)
                return 0;

        p1_len = pg_mblen(p1);
        if (pg_mblen(p2) != p1_len)
                return 0;

        /* They are the same length */
        while (p1_len--)
        {
                if (*p1++ != *p2++)
                        return 0;
        }
        return 1;
}


static char
SB_lower_char(unsigned char c, pg_locale_t locale, bool locale_is_c)
{
        if (locale_is_c)
                return pg_ascii_tolower(c);
#ifdef HAVE_LOCALE_T
        else if (locale)
                return tolower_l(c, locale);
#endif
        else
                return pg_tolower(c);
}


#define NextByte(p, plen)       ((p)++, (plen)--)

/* Set up to compile like_match.c for multibyte characters */
#define CHAREQ(p1, p2) wchareq((p1), (p2))
#define NextChar(p, plen) \
        do { int __l = pg_mblen(p); (p) +=__l; (plen) -=__l; } while (0)
#define CopyAdvChar(dst, src, srclen) \
        do { int __l = pg_mblen(src); \
                 (srclen) -= __l; \
                 while (__l-- > 0) \
                         *(dst)++ = *(src)++; \
           } while (0)

#define MatchText       MB_MatchText
//#define do_like_escape  MB_do_like_escape

#include "like_match.c"

/* Set up to compile like_match.c for single-byte characters */
#define CHAREQ(p1, p2) (*(p1) == *(p2))
#define NextChar(p, plen) NextByte((p), (plen))
#define CopyAdvChar(dst, src, srclen) (*(dst)++ = *(src)++, (srclen)--)

#define MatchText       SB_MatchText
//#define do_like_escape  SB_do_like_escape

#include "like_match.c"

/* setup to compile like_match.c for single byte case insensitive matches */
#define MATCH_LOWER(t) SB_lower_char((unsigned char) (t), locale, locale_is_c)
#define NextChar(p, plen) NextByte((p), (plen))
#define MatchText SB_IMatchText

#include "like_match.c"

/* setup to compile like_match.c for UTF8 encoding, using fast NextChar */

#define NextChar(p, plen) \
        do { (p)++; (plen)--; } while ((plen) > 0 && (*(p) & 0xC0) == 0x80 )
#define MatchText       UTF8_MatchText

#include "like_match.c"

/* Generic for all cases not requiring inline case-folding */
static inline int
GenericMatchText(char *s, int slen, char *p, int plen)
{
        if (pg_database_encoding_max_length() == 1)
                return SB_MatchText(s, slen, p, plen, 0, true);
        else if (GetDatabaseEncoding() == PG_UTF8)
                return UTF8_MatchText(s, slen, p, plen, 0, true);
        else
                return MB_MatchText(s, slen, p, plen, 0, true);
}

static inline int
Generic_Text_IC_like(text *str, text *pat, Oid collation)
{
        char       *s,
                           *p;
        int                     slen,
                                plen;
        if (pg_database_encoding_max_length() > 1)
        {
                /* lower's result is never packed, so OK to use old macros here */
                pat = DatumGetTextP(DirectFunctionCall1Coll(lower, collation,
                                                                                                        PointerGetDatum(pat)));
                p = VARDATA(pat);
                plen = (VARSIZE(pat) - VARHDRSZ);
                str = DatumGetTextP(DirectFunctionCall1Coll(lower, collation,
                                                                                                        PointerGetDatum(str)));
                s = VARDATA(str);
                slen = (VARSIZE(str) - VARHDRSZ);
                if (GetDatabaseEncoding() == PG_UTF8)
                        return UTF8_MatchText(s, slen, p, plen, 0, true);
                else
                        return MB_MatchText(s, slen, p, plen, 0, true);
        }
        else
        {

                pg_locale_t locale = 0;
                bool            locale_is_c = false;

                if (lc_ctype_is_c(collation))
                        locale_is_c = true;
                else if (collation != DEFAULT_COLLATION_OID)
                {
                        if (!OidIsValid(collation))
                        {
                                ereport(ERROR,
                                                (errcode(ERRCODE_INDETERMINATE_COLLATION),
                                                 errmsg("could not determine which collation to use for ILIKE"),
                                                 errhint("Use the COLLATE clause to set the collation explicitly.")));
                        }
                        locale = pg_newlocale_from_collation(collation);
                }

                p = VARDATA_ANY(pat);
                plen = VARSIZE_ANY_EXHDR(pat);
                s = VARDATA_ANY(str);
                slen = VARSIZE_ANY_EXHDR(str);
                return SB_IMatchText(s, slen, p, plen, locale, locale_is_c);
        }
}




PG_FUNCTION_INFO_V1(mk_varlike);

Datum
mk_varlike(PG_FUNCTION_ARGS)
{
        Null_varchar    *str = (Null_varchar *) PG_GETARG_POINTER(0);
        Null_varchar    *pat = (Null_varchar *) PG_GETARG_POINTER(1);
        bool    result,c1,c2;
        char    *s,*p;
        int     slen, plen;

        s = NVARDATA_ANY(str);
        slen = NVARSIZE_ANY_EXHDR(str);
        c1 = NVARCNST(str);
        p = NVARDATA_ANY(pat);
        plen = NVARSIZE_ANY_EXHDR(pat);
        c2 = NVARCNST(pat);
        
        /*if either is not a constant then return unknown */
        /*unless they are the same marked null*/

        if(c1 == false || c2 == false) {
            if(null_varchar_cmp(str, pat) == 0) {
                result = true;
            } else {
                PG_RETURN_NULL();
            }   
        }
        else
        {
            result = (GenericMatchText(s, slen, p, plen) == LIKE_TRUE);
        }

        PG_RETURN_BOOL(result);
}


