/*
 * 
 ******************************************************************************
  This file contains routines that can be bound to a Postgres backend and
  called by the backend in the process of processing queries.  The calling
  format for these routines is dictated by Postgres architecture.
 ******************************************************************************/

#include <inttypes.h>

#include "postgres.h"
#include "fmgr.h"

#include "access/hash.h"
#include "utils/builtins.h"
#include "utils/array.h"
#include "access/tuptoaster.h"
#include "mb/pg_wchar.h"

#include "null_varchar_data.h"

PG_MODULE_MAGIC;

#include "mk_like.c"

static void chopN(char *str, size_t n);
static int32 substr_ic(char const *str, char const *substr);

/**************************************
 * TYPMOD in function
 * Note: TYPMOD out not defined here
 * Just use pg_catalog.varchartypmodout
 *************************************/

/****************************************
 * null_anychar_typmodin
 * based on anychar_typmodin in varchar.c
 ***************************************/

static int32
null_anychar_typmodin(ArrayType *ta, const char *typename)
{
    int32       typmod;
    int32       *tl;
    int         n;

    tl = ArrayGetIntegerTypmods(ta, &n);

    if (n != 1)
        ereport(ERROR,
            (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
            errmsg("invalid type modifier")));

    if (*tl < 1)
        ereport(ERROR,
            (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
            errmsg("length for type %s must be at least 1", typename)));
        
    if (*tl > MaxAttrSize)
        ereport(ERROR,
            (errcode(ERRCODE_INVALID_PARAMETER_VALUE),
            errmsg("length for type %s cannot exceed %d",
            typename, MaxAttrSize)));

    typmod = NVARHDRSZ + *tl;

    return typmod;
}


PG_FUNCTION_INFO_V1(null_varchartypmodin);

Datum
null_varchartypmodin(PG_FUNCTION_ARGS)
{
    ArrayType  *ta = PG_GETARG_ARRAYTYPE_P(0);
    PG_RETURN_INT32(null_anychar_typmodin(ta, "null_varchar"));
}


static char * null_anychar_typmodout(int32 typmod)
{
        char       *res = (char *) palloc(64);

        if (typmod > NVARHDRSZ)
                snprintf(res, 64, "(%d)", (int) (typmod - NVARHDRSZ));
        else
                *res = '\0';

        return res;
}


PG_FUNCTION_INFO_V1(null_varchartypmodout);

Datum
null_varchartypmodout(PG_FUNCTION_ARGS)
{
        int32           typmod = PG_GETARG_INT32(0);

        PG_RETURN_CSTRING(null_anychar_typmodout(typmod));
}




/*****************************************************************************
 * Input/Output functions
 *****************************************************************************/

static Null_varchar * cstring_to_nvar_with_len(const char *s, int len)
{
    Null_varchar    *result = (Null_varchar *) palloc(len + NVARHDRSZ);
    SET_NVARSIZE(result, len + NVARHDRSZ);
    memcpy(NVARDATA(result), s, len);
    return result;
}

static Null_varchar * 
null_varchar_input(const char *s, size_t len, int32 atttypmod, bool cnst)
{
    Null_varchar     *result;
    size_t      maxlen;

    maxlen = atttypmod - NVARHDRSZ;

    if (atttypmod >= (int32) NVARHDRSZ && len > maxlen)
    {
        fprintf(stderr, gettext("should have some handling here!\n"));
        // This doesn't get hit because atttypmod is always -1 
        // Length coercion function manages length instead
    }

    result = cstring_to_nvar_with_len(s, len);
    result->cnst = cnst;

    return result;
}
    


PG_FUNCTION_INFO_V1(null_varchar_in);

Datum
null_varchar_in(PG_FUNCTION_ARGS) {
    char        *s = PG_GETARG_CSTRING(0);
    
    #ifdef NOT_USED
        Oid                     typelem = PG_GETARG_OID(1);
    #endif

    int32       atttypmod = PG_GETARG_INT32(2);
    bool constant;  
    Null_varchar     *result;
 
    if(substr_ic(s, "NULL:") == 0) {
        constant = false;
        chopN(s,5);
    } else {
        constant = true;
    }

    result = null_varchar_input(s, strlen(s), atttypmod, constant);   

    PG_RETURN_POINTER(result);
}


/*******************************************************
 * based on text_to_cstring in varchar.c
 * but to handle marked nulls
 * Would be nice if the 5 and "NULL:" weren't hard coded 
 ******************************************************/

static char * null_varchar_to_cstring(Null_varchar *nv)
{
    int         len = NVARSIZE_ANY_EXHDR(nv);
    char       *result;
    int size;

    if(!NVARCNST(nv)) {
        size = len + 5 + sizeof(bool);
        result = (char *) palloc(size);
        memcpy(result,"NULL:",5);
        memcpy(result+5,NVARDATA_ANY(nv), len);
    } else {
        size = len + sizeof(bool);
        result = (char *) palloc(size);
        memcpy(result,NVARDATA_ANY(nv), len);
    }

    result[size-1] = '\0';
    return result;
}


PG_FUNCTION_INFO_V1(null_varchar_out);

Datum
null_varchar_out(PG_FUNCTION_ARGS) 
{
    Null_varchar *null_vc = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    PG_RETURN_CSTRING(null_varchar_to_cstring(null_vc));
}


/*******************************
 * Length Coercion Function
 * compare with varchar function
 * in varchar.c
 ******************************/

PG_FUNCTION_INFO_V1(null_varchar);

Datum
null_varchar(PG_FUNCTION_ARGS)
{
    Null_varchar *source = (Null_varchar *) PG_GETARG_POINTER(0);
    int32       typmod = PG_GETARG_INT32(1);
    bool        isExplicit = PG_GETARG_BOOL(2);
    int32       len, maxlen;
    size_t      maxmblen;
    int         i;
    char        *s_data;

    len = NVARSIZE(source) - NVARHDRSZ;
    s_data = source->nv_dat;
    maxlen = typmod - NVARHDRSZ;


    /* No work if typmod is invalid or supplied data fits it already */
    if (maxlen < 0 || len <= maxlen) PG_RETURN_POINTER(source);

    //only reach here if string is too long
    fprintf(stderr, gettext("string is too long need some handling!\n"));

    /* truncate multibyte string preserving multibyte boundary */
    maxmblen = pg_mbcharcliplen(s_data, len, maxlen);

    if (!isExplicit)
    {
        for (i = maxmblen; i < len; i++)
            if (s_data[i] != ' ')
                ereport(ERROR,
                    (errcode(ERRCODE_STRING_DATA_RIGHT_TRUNCATION),
                        errmsg("value too long for type character varying(%d)",
                            maxlen)));
    }

    PG_RETURN_POINTER(cstring_to_nvar_with_len(s_data,maxmblen));
}


/******************************************************************************
 * Test for marked NULLs (group SQL nulls and constants together) 
 *****************************************************************************/

PG_FUNCTION_INFO_V1(is_null);

Datum
is_null (PG_FUNCTION_ARGS)
{
    Null_varchar    *nvar = (Null_varchar *) PG_GETARG_POINTER(0);
    int32 res;

    if(nvar == NULL || NVARCNST(nvar) == true) {
        res = 0;
    } else {
        res = 1;
    }

    PG_RETURN_BOOL(res);
}


PG_FUNCTION_INFO_V1(not_null);

Datum
not_null (PG_FUNCTION_ARGS)
{
    Null_varchar    *nvar = (Null_varchar *) PG_GETARG_POINTER(0);
    int32 res;

    if(nvar == NULL || NVARCNST(nvar) == true) {
        res = 1;
    } else {
        res = 0;
    }

    PG_RETURN_BOOL(res);
}


/************************************************************************
 * Operators, mostly altered version of text operators which varchar uses
 ***********************************************************************/

/* No collation support */
static int varchar_cmp(char *arg1, int len1, char *arg2, int len2)
{
    int result;
    result = memcmp(arg1, arg2, Min(len1, len2));

    if ((result == 0) && (len1 != len2)) result = (len1 < len2) ? -1 : 1;

    return result;
}

static int null_varchar_cmp(Null_varchar *arg1, Null_varchar *arg2)
{
    char        *a1p, *a2p;
    int         len1, len2, result;
    bool        c1, c2;

    c1 = NVARCNST(arg1);
    c2 = NVARCNST(arg2);

    if(c1 == c2) 
    {
        a1p = NVARDATA_ANY(arg1);
        a2p = NVARDATA_ANY(arg2);

        len1 = NVARSIZE_ANY_EXHDR(arg1);
        len2 = NVARSIZE_ANY_EXHDR(arg2);

        result = varchar_cmp(a1p, len1, a2p, len2);
    }
    else if (c1 == true)
    {
        //c1 a constant, c2 a mk null
        result = 1;
    }
    else
    {
        //c1 a mk null, c2 a constant
        result = -1;
    }

    return result;

}

PG_FUNCTION_INFO_V1(null_varchar_btree_cmp);

Datum 
null_varchar_btree_cmp(PG_FUNCTION_ARGS)
{
    Null_varchar    *a = (Null_varchar *) PG_GETARG_POINTER(0);
    Null_varchar    *b = (Null_varchar *) PG_GETARG_POINTER(1);

    PG_RETURN_INT32(null_varchar_cmp(a, b));
}


static Size nv_toast_raw_datum_size(Datum value)
{
    struct Null_varchar *attr = (struct Null_varchar *) DatumGetPointer(value);
    Size        result;

    if (NVARATT_IS_SHORT(attr))
    {
        /*
         * we have to normalize the header length to NVARHDRSZ or else the
         * callers of this function will be confused.
         */
        result = NVARSIZE_SHORT(attr) - NVARHDRSZ_SHORT + NVARHDRSZ;     
    }
    else
    {
        /* plain untoasted datum */
        result = NVARSIZE(attr);
    }
    return result;
}


PG_FUNCTION_INFO_V1(null_varchareq);

Datum
null_varchareq(PG_FUNCTION_ARGS)
{
    Datum       arg1 = PG_GETARG_DATUM(0);
    Datum       arg2 = PG_GETARG_DATUM(1);
    bool        result, c1, c2;
    Size        len1, len2;

    /*
     * assuming dangerously here than we have plain 
     * untoasted data
     */
    len1 = nv_toast_raw_datum_size(arg1);
    len2 = nv_toast_raw_datum_size(arg2);

    c1 = NVARCNST(arg1);
    c2 = NVARCNST(arg2);

    if (len1 != len2)   
    {
        result = false;
    }
    else if(c1 != c2) 
    {
        result = false;
    }
    else
    {
        result = (memcmp(NVARDATA_ANY(arg1), NVARDATA_ANY(arg2),len1 - NVARHDRSZ) == 0);

        //for cleaning up toasting. Which isn't yet supported
        //PG_FREE_IF_COPY(targ1, 0);
        //PG_FREE_IF_COPY(targ2, 1);
    }

    PG_RETURN_BOOL(result);
}
        

PG_FUNCTION_INFO_V1(null_varcharne);

Datum
null_varcharne(PG_FUNCTION_ARGS)
{
    Datum       arg1 = PG_GETARG_DATUM(0);
    Datum       arg2 = PG_GETARG_DATUM(1);
    bool        result, c1, c2;
    Size        len1, len2;

    len1 = nv_toast_raw_datum_size(arg1);
    len2 = nv_toast_raw_datum_size(arg2);

    c1 = NVARCNST(arg1);
    c2 = NVARCNST(arg2);

    if(len1 != len2) result = true;

    else if(c1 != c2) result = true;

    else result = (memcmp(NVARDATA_ANY(arg1), NVARDATA_ANY(arg2),len1 - NVARHDRSZ) != 0);

    PG_RETURN_BOOL(result);

}


PG_FUNCTION_INFO_V1(null_varcharlt);

Datum
null_varcharlt(PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    bool        result;

    result = (null_varchar_cmp(arg1, arg2) < 0);
    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varcharle);

Datum
null_varcharle(PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    bool        result;

    result = (null_varchar_cmp(arg1, arg2) <= 0);
    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchargt);

Datum
null_varchargt(PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    bool        result;

    result = (null_varchar_cmp(arg1, arg2) > 0);
    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varcharge);

Datum
null_varcharge(PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    bool        result;

    result = (null_varchar_cmp(arg1, arg2) >= 0);
    PG_RETURN_BOOL(result);
}


/***************************************************
 * Operators which can return unknown for operations
 * which involve marked nulls
 * ************************************************/

// -1 if arg1 less than arg2
//  1 if arg1 greater than arg2
//  0 if equal
// -2 if unknown

static int null_varchar_mk_cmp(Null_varchar *arg1, Null_varchar *arg2)
{
    char        *a1p, *a2p;
    int         len1, len2, result;
    bool        c1, c2;

    c1 = NVARCNST(arg1);
    c2 = NVARCNST(arg2);

    if(c1 && c2)
    {
        //both arg1 and arg2 are constants
        a1p = NVARDATA_ANY(arg1);
        a2p = NVARDATA_ANY(arg2);

        len1 = NVARSIZE_ANY_EXHDR(arg1);
        len2 = NVARSIZE_ANY_EXHDR(arg2);

        result = varchar_cmp(a1p, len1, a2p, len2);
    }
    else if (c1 == false && c2 == false )
    {
        //both arg1 and arg2 are marked nulls
        //need to check if they are the same marked null
        a1p = NVARDATA_ANY(arg1);
        a2p = NVARDATA_ANY(arg2);

        len1 = NVARSIZE_ANY_EXHDR(arg1);
        len2 = NVARSIZE_ANY_EXHDR(arg2);

        if(0 == varchar_cmp(a1p, len1, a2p, len2)) 
            result = 0;
        else
            result = -2;
    }
    else
    {
        result = -2;
    }

    return result;
}

PG_FUNCTION_INFO_V1(null_varchar_mkeq);

Datum
null_varchar_mkeq (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2) 
        PG_RETURN_NULL();
    else
        result = (cmp == 0);

    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchar_mkne);

Datum
null_varchar_mkne (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2)
        PG_RETURN_NULL();
    else
        result = (cmp != 0);

    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchar_mklt);

Datum
null_varchar_mklt (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2)
        PG_RETURN_NULL();
    else
        result = (cmp < 0);

    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchar_mkle);

Datum
null_varchar_mkle (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2)
        PG_RETURN_NULL();
    else
        result = (cmp <= 0);

    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchar_mkge);

Datum
null_varchar_mkge (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2)
        PG_RETURN_NULL();
    else
        result = (cmp >= 0);

    PG_RETURN_BOOL(result);
}


PG_FUNCTION_INFO_V1(null_varchar_mkgt);

Datum
null_varchar_mkgt (PG_FUNCTION_ARGS)
{
    Null_varchar *arg1 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Null_varchar *arg2 = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(1));
    int         cmp;
    bool        result;

    cmp = null_varchar_mk_cmp(arg1, arg2);

    if(cmp == -2)
        PG_RETURN_NULL();
    else
        result = (cmp > 0);

    PG_RETURN_BOOL(result);
}


/************************
 * Hashing
 ***********************/

PG_FUNCTION_INFO_V1(hash_null_varchar);

Datum
hash_null_varchar(PG_FUNCTION_ARGS)
{
    Null_varchar *key = (Null_varchar *) DatumGetPointer(PG_GETARG_DATUM(0));
    Datum result;

    result = hash_any((unsigned char *) NVARDATA_ANY(key),NVARSIZE_ANY_EXHDR(key));

    return result;
}


/******************************************************************************
 * helper function definitions
 * ToDo: put these function in separate file and include .h file
 ******************************************************************************/


/* Remove first n chars from String */
static void chopN(char *str, size_t n)
{
    size_t len = strlen(str);
    if (n > len)
        return;  // Or: n = len;
        memmove(str, str+n, len - n + 1);
}

/* check for a substring ignoring case */
static int32 substr_ic(char const *str, char const *substr)
{
    int lens, i;
    lens = strlen(substr);

    if(strlen(str) < lens) {
        return -1;
    }

    for (i = 0;i < lens; str++, substr++, i++) {
        int d = tolower(*str) - tolower(*substr);
        if (d != 0) {
            return d;
        }
    }


    return 0;
}

