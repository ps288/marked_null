/********************************************************************
 * Null_varchar type Based on varlen but need the extra boolean field 
 * to check for marked null/constant
 *******************************************************************/

typedef struct Null_varchar
{
    char        nv_len[4];
    bool        cnst;
    char        nv_dat[FLEXIBLE_ARRAY_MEMBER];
}    Null_varchar;

typedef struct
{
    uint32      nva_header;
    bool        nva_const;
    char        nva_data[FLEXIBLE_ARRAY_MEMBER];
} nvarattrib_4b;

typedef struct
{
    uint8       nva_header;
    bool        nva_const;
    char        nva_data[FLEXIBLE_ARRAY_MEMBER]; /* Data begins here */
} nvarattrib_1b;



#define NVARHDRSZ (VARHDRSZ + sizeof(bool))
#define NVARHDRSZ_SHORT                  offsetof(nvarattrib_1b, nva_data)
#define SET_NVARSIZE(NV_PTR, len) (((nvarattrib_4b *) (NV_PTR))->nva_header = (((uint32) (len)) << 2))
#define NVARDATA(NV_PTR) (NV_PTR)->nv_dat
#define NVARDATA_4B(NV_PTR)         (((nvarattrib_4b *) (NV_PTR))->nva_data)
#define NVARDATA_1B(NV_PTR)         (((nvarattrib_1b *) (NV_PTR))->nva_data)
#define NVARSIZE(NV_PTR) ((((nvarattrib_4b *) (NV_PTR))->nva_header >> 2) & 0x3FFFFFFF)
#define NVARCNST_1B(NV_PTR)        (((nvarattrib_1b *) (NV_PTR))->nva_const)
#define NVARCNST_4B(NV_PTR)        (((nvarattrib_4b *) (NV_PTR))->nva_const)


#define NVARATT_IS_1B(PTR) \
        ((((nvarattrib_1b *) (PTR))->nva_header & 0x01) == 0x01)

#define NVARATT_IS_SHORT(NV_PTR)                            NVARATT_IS_1B(NV_PTR)

#define NVARDATA_ANY(PTR) \
         (NVARATT_IS_1B(PTR) ? NVARDATA_1B(PTR) : NVARDATA_4B(PTR))

#define NVARCNST(PTR) \
        (NVARATT_IS_1B(PTR) ? NVARCNST_1B(PTR) : NVARCNST_4B(PTR))

#define NVARATT_IS_1B_E(PTR) \
        ((((nvarattrib_1b *) (PTR))->nva_header) == 0x01)

#define NVARSIZE_1B(PTR) \
        ((((nvarattrib_1b *) (PTR))->nva_header >> 1) & 0x7F)

#define NVARSIZE_4B(PTR) \
        ((((nvarattrib_4b *) (PTR))->nva_header >> 2) & 0x3FFFFFFF)

#define NVARSIZE_SHORT(PTR)                                      NVARSIZE_1B(PTR)

#define NVARSIZE_ANY_EXHDR(PTR) \
         (NVARATT_IS_1B(PTR) ? NVARSIZE_1B(PTR)-NVARHDRSZ_SHORT : \
          NVARSIZE_4B(PTR)-NVARHDRSZ)


/*Toasting Macros */
#define NVARATT_IS_EXTERNAL(PTR)                         NVARATT_IS_1B_E(PTR)

#define NVARTAG_EXTERNAL(PTR)                            NVARTAG_1B_E(PTR)

static int null_varchar_cmp(Null_varchar *arg1, Null_varchar *arg2);
