--TYpemod function(s)

CREATE FUNCTION null_varchartypmodin(cstring[])
   RETURNS integer
   AS 'null_varchar', 'null_varchartypmodin'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchartypmodout(integer)
   RETURNS cstring
   AS 'null_varchar', 'null_varchartypmodout'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

--I/O functions

CREATE FUNCTION null_varchar_in(cstring)
   RETURNS null_varchar
   AS 'null_varchar', 'null_varchar_in'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_out(null_varchar)
   RETURNS cstring
   AS 'null_varchar', 'null_varchar_out'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

-- TYPE definition

CREATE TYPE null_varchar (
   --internallength = VARIABLE,
   input = null_varchar_in,
   output = null_varchar_out,
   LIKE = pg_catalog.varchar,
   typmod_in =  null_varchartypmodin,
   typmod_out = null_varchartypmodout    
);

--like function

CREATE FUNCTION like(null_varchar,null_varchar)
    RETURNS bool
    AS 'null_varchar', 'mk_varlike'
    LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

--like operator

CREATE OPERATOR ~~ (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = like,
    restrict = likesel,
    join = likejoinsel
);


--Length coercion cast

CREATE FUNCTION null_varchar(null_varchar, integer, boolean)
    RETURNS null_varchar
    AS 'null_varchar', 'null_varchar'
    LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE CAST (null_varchar AS null_varchar)
    WITH FUNCTION null_varchar(null_varchar, integer, boolean)
    AS IMPLICIT;

--"Correct" functions

CREATE FUNCTION null_varchar_mkeq(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mkeq'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_mkne(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mkne'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_mklt(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mklt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_mkle(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mkle'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_mkgt(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mkgt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchar_mkge(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchar_mkge'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

--marked null testing functions

CREATE FUNCTION is_null(null_varchar)
    RETURNS bool
    AS 'null_varchar', 'is_null'
    LANGUAGE C IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION not_null(null_varchar)
    RETURNS bool
    AS 'null_varchar', 'not_null'
    LANGUAGE C IMMUTABLE PARALLEL SAFE;

CREATE OPERATOR @@ (
    leftarg = null_varchar,
    procedure = is_null,
    restrict = mknull_sel,
    negator = @!
);

CREATE OPERATOR @! (
    leftarg = null_varchar,
    procedure = not_null,
    restrict = mk_not_null_sel,
    negator = @@
);


--define operators

CREATE OPERATOR = (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mkeq,
    commutator = = ,
    negator = <> ,
    restrict = mk_eqsel,
    join = mk_eqjoinsel,
    --HASHES,
    MERGES
);

CREATE OPERATOR <> (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mkne,
    commutator = <> ,
    negator = =,
    restrict = mk_neqsel,
    join = mk_neqjoinsel
    );

CREATE OPERATOR < (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mklt,
    commutator = >,
    negator = >=
);

CREATE OPERATOR <= (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mkle,
    commutator = >=,
    negator = >
);

CREATE OPERATOR >= (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mkge,
    commutator = <=,
    negator = <
);

CREATE OPERATOR > (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchar_mkgt,
    commutator = <,
    negator = <=
);

--functions for Indexing operators

CREATE FUNCTION null_varchareq(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchareq'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varcharne(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varcharne'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varcharlt(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varcharlt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varcharle(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varcharle'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varchargt(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varchargt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_varcharge(null_varchar, null_varchar)
   RETURNS bool
   AS 'null_varchar', 'null_varcharge'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

-- Indexing operators

CREATE OPERATOR =& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchareq,
    commutator = =&,
    negator = <>&,
    restrict = eqsel,
    join = eqjoinsel,
    MERGES,
    HASHES
);

CREATE OPERATOR <>& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varcharne,
    commutator = <>& ,
    negator = =& ,
    restrict = neqsel,
    join = neqjoinsel
);

CREATE OPERATOR <& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varcharlt,
    commutator = >&,
    negator = >=&,
    restrict = scalarltsel,
    join = scalarltjoinsel
);

CREATE OPERATOR <=& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varcharle,
    commutator = >=&,
    negator = >&,
    restrict = scalarltsel,
    join = scalarltjoinsel
);

CREATE OPERATOR >=& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varcharge,
    commutator = <=&,
    negator = <&,
    restrict = scalargtsel,
    join = scalargtjoinsel
);

CREATE OPERATOR >& (
    leftarg = null_varchar,
    rightarg = null_varchar,
    procedure = null_varchargt,
    commutator = <&,
    negator = <=&,
    restrict = scalargtsel,
    join = scalargtjoinsel
);

--btree support 

CREATE FUNCTION null_varchar_btree_cmp(null_varchar, null_varchar)
   RETURNS int4
   AS 'null_varchar', 'null_varchar_btree_cmp'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE OPERATOR CLASS null_varchar_ops
    DEFAULT FOR TYPE null_varchar USING btree AS
    OPERATOR    1       <& ,
    OPERATOR    2       <=& ,
    OPERATOR    3       = ,
    OPERATOR    4       >=& ,
    OPERATOR    5       >& ,
    FUNCTION    1       null_varchar_btree_cmp(null_varchar, null_varchar);

/*
CREATE OPERATOR CLASS null_varchar_ops_twov
    FOR TYPE null_varchar USING btree AS
    OPERATOR    1       <& ,
    OPERATOR    2       <=& ,
    OPERATOR    3       =& ,
    OPERATOR    4       >=& ,
    OPERATOR    5       >& ,
    FUNCTION    1       null_varchar_btree_cmp(null_varchar, null_varchar);
*/

--hashing

CREATE FUNCTION hash_null_varchar(null_varchar)
        RETURNS int4
        AS 'null_varchar', 'hash_null_varchar'
        LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE OPERATOR CLASS null_varchar_ops
    DEFAULT FOR TYPE null_varchar USING hash AS
    OPERATOR    1       = ,
    FUNCTION    1       hash_null_varchar(null_varchar);
/*
CREATE OPERATOR CLASS null_varchar_ops_twov
    FOR TYPE null_varchar USING hash AS
    OPERATOR    1       =& ,
    FUNCTION    1       hash_null_varchar(null_varchar);
*/
