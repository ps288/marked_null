/*
 * 
 ******************************************************************************
  This file contains routines that can be bound to a Postgres backend and
  called by the backend in the process of processing queries.  The calling
  format for these routines is dictated by Postgres architecture.
 ******************************************************************************/

#include "postgres.h"
#include "fmgr.h"

#include "access/hash.h"
#include "utils/builtins.h"
#include "utils/array.h"

PG_MODULE_MAGIC;

typedef struct Null_integer
{
	int32	value;
	bool	cnst;
}	Null_integer;


/****************************************************************************
 * helper function declarations
 ****************************************************************************/ 

static void chopN(char *str, size_t n);
static int32 non_numbers(const char *s);
static int32 substr_ic(char const *str, char const *substr);

/*****************************************************************************
 * Input/Output functions
 *****************************************************************************/

PG_FUNCTION_INFO_V1(null_integer_in);

Datum 
null_integer_in(PG_FUNCTION_ARGS)
{
        char       *str = PG_GETARG_CSTRING(0);
	bool constant;
        Null_integer *result;

	if(substr_ic(str, "NULL:") == 0) {
		constant = false;
		chopN(str,5);
	} else {
		constant = true;
	}

        //pg_atoi checks this too, but here we can say there is a null_integer error
        //rather than an integer error
	if(non_numbers(str) || 0 == strlen(str)) {
		ereport(ERROR,
                                (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
                                 errmsg("invalid input syntax for null_integer: \"%s%s\"",
                                                (constant == false)?"NULL:":"", 
						str)));
	} 

	result = (Null_integer *) palloc(sizeof(Null_integer));
	result->value = pg_atoi(str, sizeof(int32), '\0');
	result->cnst = constant;

	PG_RETURN_POINTER(result);
}


PG_FUNCTION_INFO_V1(null_integer_out);

Datum
null_integer_out(PG_FUNCTION_ARGS)
{
	Null_integer *null_int = (Null_integer *) PG_GETARG_POINTER(0);
	char       *result;
        
        if(null_int == NULL) {
            /*
            ereport(ERROR,
                                (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
                                 errmsg("returning null")));
            */
            fprintf(stderr, gettext("returning null from out part!\n"));
            PG_RETURN_NULL();
        }

	if(null_int->cnst) {
		result = psprintf("%d",null_int->value);
	} else {
		result = psprintf("NULL:%d",null_int->value);	
	}

	PG_RETURN_CSTRING(result);
}

/************************
 * Casting Functions(s)
 ************************/

PG_FUNCTION_INFO_V1(null_integer_cast);

Datum
null_integer_cast(PG_FUNCTION_ARGS)
{
    Null_integer *result;
    int32           arg1 = PG_GETARG_INT32(0);
    result = (Null_integer *) palloc(sizeof(Null_integer));
    
    result->cnst = true;
    result->value = arg1;

    PG_RETURN_POINTER(result);

}

PG_FUNCTION_INFO_V1(integer_cast);

Datum
integer_cast(PG_FUNCTION_ARGS)
{
    Null_integer *null_int = (Null_integer *) PG_GETARG_POINTER(0);
    
    if(null_int->cnst == true) {
        PG_RETURN_INT32(null_int->value);
    } else {
        PG_RETURN_NULL();
    }
}     


/******************************************************************************
 * add subtract multiply etc.
 ******************************************************************************/

PG_FUNCTION_INFO_V1(null_integer_add);

Datum 
null_integer_add(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);	
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);
    Null_integer    *result;

    result = (Null_integer *) palloc(sizeof(Null_integer));

    if(a->cnst && b->cnst) {
        result->cnst = true;
        result->value = a->value + b->value;
    } else {
        //this part is garbage, just to show it's working
        PG_RETURN_NULL();
    }

    PG_RETURN_POINTER(result);
}    
                    

PG_FUNCTION_INFO_V1(null_integer_subtract);

Datum
null_integer_subtract(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);
    Null_integer    *result;

    result = (Null_integer *) palloc(sizeof(Null_integer));

    if(a->cnst && b->cnst) {
        result->cnst = true;
        result->value = a->value - b->value;
    } else {
        //this part is garbage, just to show it's working
        PG_RETURN_NULL();
    }

    PG_RETURN_POINTER(result);
}

/********************
 * aggregates
 *******************/

typedef struct Int8TransTypeData
{
        int64           count;
        int64           sum;
} Int8TransTypeData;

PG_FUNCTION_INFO_V1(null_int4_avg_accum);

Datum
null_int4_avg_accum(PG_FUNCTION_ARGS)
{
    ArrayType           *transarray;
    Int8TransTypeData   *transdata;
    Null_integer        *newval = (Null_integer *) PG_GETARG_POINTER(1);

    if (AggCheckCallContext(fcinfo, NULL))
        transarray = PG_GETARG_ARRAYTYPE_P(0);
    else
        transarray = PG_GETARG_ARRAYTYPE_P_COPY(0);

    if (ARR_HASNULL(transarray) ||
        ARR_SIZE(transarray) != ARR_OVERHEAD_NONULLS(1) + sizeof(Int8TransTypeData))
        elog(ERROR, "expected 2-element int8 array");

    if(newval->cnst) {
        transdata = (Int8TransTypeData *) ARR_DATA_PTR(transarray);
        transdata->count++;
        transdata->sum += newval->value;
    }


    PG_RETURN_ARRAYTYPE_P(transarray);
}


PG_FUNCTION_INFO_V1(int4_avg_combine);

Datum
int4_avg_combine(PG_FUNCTION_ARGS)
{
        ArrayType  *transarray1;
        ArrayType  *transarray2;
        Int8TransTypeData *state1;
        Int8TransTypeData *state2;

        if (!AggCheckCallContext(fcinfo, NULL))
                elog(ERROR, "aggregate function called in non-aggregate context");

        transarray1 = PG_GETARG_ARRAYTYPE_P(0);
        transarray2 = PG_GETARG_ARRAYTYPE_P(1);

        if (ARR_HASNULL(transarray1) ||
                ARR_SIZE(transarray1) != ARR_OVERHEAD_NONULLS(1) + sizeof(Int8TransTypeData))
                elog(ERROR, "expected 2-element int8 array");

        if (ARR_HASNULL(transarray2) ||
                ARR_SIZE(transarray2) != ARR_OVERHEAD_NONULLS(1) + sizeof(Int8TransTypeData))
                elog(ERROR, "expected 2-element int8 array");

        state1 = (Int8TransTypeData *) ARR_DATA_PTR(transarray1);
        state2 = (Int8TransTypeData *) ARR_DATA_PTR(transarray2);

        state1->count += state2->count;
        state1->sum += state2->sum;

        PG_RETURN_ARRAYTYPE_P(transarray1);
}


/******************************************************************************
 * Test for marked NULLs (group SQL nulls and constants together) 
 ******************************************************************************/

PG_FUNCTION_INFO_V1(is_null);

Datum
is_null (PG_FUNCTION_ARGS)
{
    Null_integer    *nint = (Null_integer *) PG_GETARG_POINTER(0);
    int32 res;

    if(nint == NULL || nint->cnst == true) {
        res = 0;
    } else {
        res = 1;
    }

    PG_RETURN_BOOL(res);
}


PG_FUNCTION_INFO_V1(not_null);

Datum
not_null (PG_FUNCTION_ARGS)
{
    Null_integer    *nint = (Null_integer *) PG_GETARG_POINTER(0);
    int32 res;
    
    if(nint == NULL || nint->cnst == true) {
        res = 1;
    } else {
        res = 0;
    }

    PG_RETURN_BOOL(res);
}


/******************************************************************************
 * Simple operators
 ******************************************************************************/

PG_FUNCTION_INFO_V1(null_integer_eq);

Datum
null_integer_eq (PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);
    int32 res;

    if(a->cnst == b->cnst && a->value == b->value) {
        res = 1;
    } else if (a->cnst == true && b->cnst == true && a->value != b->value){
        res = 0;
    } else {
        /*
        ereport(DEBUG,
                                (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION),
                                 errmsg("returning null")));
        */
 
        PG_RETURN_NULL();
        
    }

    PG_RETURN_BOOL(res);
}


PG_FUNCTION_INFO_V1(null_integer_ne);

Datum
null_integer_ne (PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);
    int32 res;

    if(a->cnst == b->cnst && a->value == b->value) {
        res = 0;
    } else if (a->cnst == true && b->cnst == true && a->value != b->value){
        res = 1;
    } else {
        PG_RETURN_NULL();
    }

    PG_RETURN_BOOL(res);
}

// returns:
// -1 if a less than b
// 1  if a greater than b
// 0  if equal
// -2 if unknown

static int
null_integer_mk_cmp(Null_integer *a, Null_integer *b)
{
    if(a->cnst == true && b->cnst == true) {

        if (a->value < b->value) {
            //both constants
            return -1;
        } else if (a->value > b->value) {
            return 1;
        } else {
            return 0;
        }

    } else if (a->cnst == false && b->cnst == false && a->value == b->value) {
        //if both the same marked null
        return 0;
    } else {
        return -2;
    } 

}

PG_FUNCTION_INFO_V1(null_integer_lt);

Datum
null_integer_lt(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    int res = null_integer_mk_cmp(a, b);
    if (res == -2) PG_RETURN_NULL();

    PG_RETURN_BOOL(res < 0);
}


PG_FUNCTION_INFO_V1(null_integer_le);

Datum
null_integer_le(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    int res = null_integer_mk_cmp(a, b);
    if (res == -2) PG_RETURN_NULL();

    PG_RETURN_BOOL(res <= 0);
}


PG_FUNCTION_INFO_V1(null_integer_ge);

Datum
null_integer_ge(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    int res = null_integer_mk_cmp(a, b);
    if (res == -2) PG_RETURN_NULL();

    PG_RETURN_BOOL(res >= 0);
}


PG_FUNCTION_INFO_V1(null_integer_gt);

Datum
null_integer_gt(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    int res = null_integer_mk_cmp(a, b);
    if (res == -2) PG_RETURN_NULL();

    PG_RETURN_BOOL(res > 0);
}


/*****************************************************************************
 * B-tree stuff
 *****************************************************************************/

static int
null_integer_cmp(Null_integer *a, Null_integer *b)
{
    if(a->cnst == b->cnst) {
        
        if (a->value < b->value) {
            //both are constants/marked nulls
            return -1;
        } else if (a->value > b->value) {
            return 1;
        } else {
            return 0;
        }

    } else if (a->cnst){
        //a is a constant, b a marked null
        return 1;
    } else {
        //b is a constant, a is a marked null
        return -1;
    }
}


PG_FUNCTION_INFO_V1(null_integer_btree_lt);

Datum
null_integer_btree_lt(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) < 0);
}


PG_FUNCTION_INFO_V1(null_integer_btree_le);

Datum
null_integer_btree_le(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) <= 0);
}


PG_FUNCTION_INFO_V1(null_integer_btree_eq);

Datum
null_integer_btree_eq(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) == 0);
}

PG_FUNCTION_INFO_V1(null_integer_btree_ne);

Datum
null_integer_btree_ne(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) != 0);
}

PG_FUNCTION_INFO_V1(null_integer_btree_ge);

Datum
null_integer_btree_ge(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) >= 0);
}


PG_FUNCTION_INFO_V1(null_integer_btree_gt);

Datum
null_integer_btree_gt(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_BOOL(null_integer_cmp(a, b) > 0);
}


PG_FUNCTION_INFO_V1(null_integer_btree_cmp);

Datum
null_integer_btree_cmp(PG_FUNCTION_ARGS)
{
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    Null_integer    *b = (Null_integer *) PG_GETARG_POINTER(1);

    PG_RETURN_INT32(null_integer_cmp(a, b));
}


/************
 * hashing
 ***********/

PG_FUNCTION_INFO_V1(hash_null_integer);

Datum
hash_null_integer(PG_FUNCTION_ARGS) {
    Null_integer    *a = (Null_integer *) PG_GETARG_POINTER(0);
    return hash_uint32(a->value);
}




/******************************************************************************
 * helper function definitions
 ******************************************************************************/

//Remove first n chars from String 
static void chopN(char *str, size_t n)
{
    size_t len = strlen(str);
    if (n > len)
        return;  // Or: n = len;
	memmove(str, str+n, len - n + 1);
}

// check if string contains non digits 
static int32 non_numbers(const char *s)
{
    if(*s == '-') s++;    

    while (*s) {
        if (isdigit(*s++) == 0) return 1;
    }
    return 0;
}

// check for a substring ignoring case 
static int32 substr_ic(char const *str, char const *substr)
{
    int lens, i;
    lens = strlen(substr);

    if(strlen(str) < lens) {
        return -1;
    }

    for (i = 0;i < lens; str++, substr++, i++) {
        int d = tolower(*str) - tolower(*substr);
        if (d != 0) {
            return d;
        }
    }
    
    return 0;
}
