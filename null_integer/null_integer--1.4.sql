-- IN/OUT functions

CREATE FUNCTION null_integer_in(cstring)
   RETURNS null_integer
   AS 'null_integer', 'null_integer_in'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_out(null_integer)
   RETURNS cstring
   AS 'null_integer', 'null_integer_out'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

-- TYPE definition

CREATE TYPE null_integer (
   internallength = 8,
   input = null_integer_in,
   output = null_integer_out
);


-- Cast Function

CREATE FUNCTION null_integer_cast(integer)
    RETURNS null_integer
    AS 'null_integer', 'null_integer_cast'
     LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION integer_cast(null_integer)
    RETURNS integer
    AS 'null_integer', 'integer_cast'
     LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE CAST (integer AS null_integer)
    WITH FUNCTION null_integer_cast(integer)
    AS IMPLICIT;

CREATE CAST (null_integer AS integer)
    WITH FUNCTION integer_cast(null_integer);


-- "Correct" functions

CREATE FUNCTION null_integer_add(null_integer, null_integer)
   RETURNS null_integer
   AS 'null_integer', 'null_integer_add'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_subtract(null_integer, null_integer)
   RETURNS null_integer
   AS 'null_integer', 'null_integer_subtract'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_eq(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_eq'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_ne(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_ne'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_lt(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_lt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_le(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_le'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_ge(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_ge'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_gt(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_gt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

--aggregate helper functions

CREATE FUNCTION null_int4_avg_accum(_int8, null_integer)
    RETURNS _int8
    AS 'null_integer', 'null_int4_avg_accum'
    LANGUAGE C IMMUTABLE STRICT;


CREATE FUNCTION int4_avg_combine(_int8, _int8)
    RETURNS _int8
    AS 'null_integer', 'int4_avg_combine'
    LANGUAGE C IMMUTABLE STRICT;

--aggregate

CREATE AGGREGATE avg (null_integer)
(
    SFUNC = null_int4_avg_accum,
    STYPE = _int8,
    FINALFUNC = int8_avg,
    COMBINEFUNC = int4_avg_combine,
    INITCOND = '{0,0}'
);

--marked null testing functions

CREATE FUNCTION is_null(null_integer)
    RETURNS bool
    AS 'null_integer', 'is_null'
    LANGUAGE C IMMUTABLE PARALLEL SAFE;

CREATE FUNCTION not_null(null_integer)
    RETURNS bool
    AS 'null_integer', 'not_null'
    LANGUAGE C IMMUTABLE PARALLEL SAFE;

CREATE OPERATOR @@ (
    leftarg = null_integer,
    procedure = is_null,
    restrict = mknull_sel,
    negator = @!
);

CREATE OPERATOR @! (
    leftarg = null_integer,
    procedure = not_null,
    restrict = mk_not_null_sel,
    negator = @@
);

-- define operators

CREATE OPERATOR + (
   leftarg = null_integer,
   rightarg = null_integer,
   procedure = null_integer_add,
   commutator = +
);

CREATE OPERATOR - (
   leftarg = null_integer,
   rightarg = null_integer,
   procedure = null_integer_subtract
);


CREATE OPERATOR = (
    leftarg = null_integer, 
    rightarg = null_integer, 
    procedure = null_integer_eq,
    commutator = = ,
    negator = <> ,
    restrict = mk_eqsel,
    join = mk_eqjoinsel,
    HASHES,
    MERGES
);

CREATE OPERATOR <> (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_ne,
    commutator = <> ,
    negator = =,
    restrict = mk_neqsel,
    join = mk_neqjoinsel 
    );

CREATE OPERATOR < (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_lt,
    commutator = >,
    negator = >=
);

CREATE OPERATOR <= (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_le,
    commutator = >=,
    negator = >
);

CREATE OPERATOR >= (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_ge,
    commutator = <=,
    negator = <
);

CREATE OPERATOR > (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_gt,
    commutator = <,
    negator = <=
);

--functions for Indexing operators

CREATE FUNCTION null_integer_btree_lt(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_lt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_btree_le(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_le'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;


CREATE FUNCTION null_integer_btree_eq(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_eq'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_btree_ne(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_ne'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_btree_ge(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_ge'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE FUNCTION null_integer_btree_gt(null_integer, null_integer)
   RETURNS bool
   AS 'null_integer', 'null_integer_btree_gt'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

-- Indexing operators

CREATE OPERATOR <& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_lt,
    commutator = >&,
    negator = >=&,
    restrict = scalarltsel,
    join = scalarltjoinsel
);

CREATE OPERATOR >=& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_ge,
    commutator = <=&,
    negator = <&,
    restrict = scalargtsel,
    join = scalargtjoinsel
);

CREATE OPERATOR <=& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_le,
    commutator = >=&,
    negator = >&,
    restrict = scalarltsel,
    join = scalarltjoinsel
);


CREATE OPERATOR =& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_eq,
    commutator = =&,
    negator = <>&,
    restrict = eqsel,
    join = eqjoinsel,
    HASHES,
    MERGES
);



CREATE OPERATOR <>& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_ne,
    commutator = <>& ,
    negator = =& ,
    restrict = neqsel,
    join = neqjoinsel
    );


CREATE OPERATOR >& (
    leftarg = null_integer,
    rightarg = null_integer,
    procedure = null_integer_btree_gt,
    commutator = <&,
    negator = <=&,
    restrict = scalargtsel,
    join = scalargtjoinsel
);

--btree support

CREATE FUNCTION null_integer_btree_cmp(null_integer, null_integer)
   RETURNS int4
   AS 'null_integer', 'null_integer_btree_cmp'
   LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE OPERATOR CLASS null_integer_ops
    DEFAULT FOR TYPE null_integer USING btree AS
    OPERATOR    1       <& ,
    OPERATOR    2       <=& ,
    OPERATOR    3       = ,
    OPERATOR    4       >=& ,
    OPERATOR    5       >& ,
    FUNCTION    1       null_integer_btree_cmp(null_integer, null_integer);

/*
CREATE OPERATOR CLASS null_integer_ops_twov
    FOR TYPE null_integer USING btree AS
    OPERATOR    1       <& ,
    OPERATOR    2       <=& ,
    OPERATOR    3       =& ,
    OPERATOR    4       >=& ,
    OPERATOR    5       >& ,
    FUNCTION    1       null_integer_btree_cmp(null_integer, null_integer);
*/

--hashing

CREATE FUNCTION hash_null_integer(null_integer)
        RETURNS int4
        AS 'null_integer', 'hash_null_integer'
        LANGUAGE C IMMUTABLE STRICT PARALLEL SAFE;

CREATE OPERATOR CLASS null_integer_ops
    DEFAULT FOR TYPE null_integer USING hash AS
    OPERATOR    1       = ,
    FUNCTION    1       hash_null_integer(null_integer);
/*
CREATE OPERATOR CLASS null_integer_ops_twov
    FOR TYPE null_integer USING hash AS
    OPERATOR    1       =& ,
    FUNCTION    1       hash_null_integer(null_integer);
*/
