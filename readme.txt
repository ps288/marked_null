Project to add new data types to PostgreSQL which allow the use of marked nulls

Currently there are 3 extensions:

mk_restrict
null_varachar
null_integer

Note that to create the null_varchar and null_integer extension using the CREATE EXTENSION syntax
one must first CREATE the mk_restrict EXTENSION as they rely on it.
