--restrict functions

CREATE FUNCTION mk_eqsel(internal, oid, internal, integer)
   RETURNS float8
   AS 'mk_restrict', 'mk_eqsel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;

CREATE FUNCTION mk_neqsel(internal, oid, internal, integer)
   RETURNS float8
   AS 'mk_restrict', 'mk_neqsel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;

CREATE FUNCTION mknull_sel(internal, oid, internal, integer)
   RETURNS float8
   AS 'mk_restrict', 'mknull_sel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;

CREATE FUNCTION mk_not_null_sel(internal, oid, internal, integer)
   RETURNS float8
   AS 'mk_restrict', 'mk_not_null_sel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;

--join functions

CREATE FUNCTION mk_eqjoinsel(internal, oid, internal, int2, internal)
   RETURNS float8
   AS 'mk_restrict', 'mk_eqjoinsel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;

CREATE FUNCTION mk_neqjoinsel(internal, oid, internal, int2, internal)
   RETURNS float8
   AS 'mk_restrict', 'mk_neqjoinsel'
   LANGUAGE C STABLE STRICT PARALLEL SAFE;



