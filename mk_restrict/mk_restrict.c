/******************************************************************
 * File attempts to implement some of the restrict and join clauses
 * The originals do not like the possibility of null output on
 * non null input. Here we allow for that
******************************************************************/

#include "postgres.h"
#include "fmgr.h"

#include "nodes/nodeFuncs.h"
#include "utils/selfuncs.h"
#include "catalog/pg_statistic.h"
#include "access/htup_details.h"
#include "catalog/pg_collation.h"
#include "utils/lsyscache.h"
#include "optimizer/pathnode.h"


PG_MODULE_MAGIC;

static Datum mk_FunctionCall2Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2);

static Datum mk_DirectFunctionCall4Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2, Datum arg3, Datum arg4);

static Datum mk_DirectFunctionCall5Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2, Datum arg3, Datum arg4, Datum arg5);


static double var_mk_eq_const(VariableStatData *vardata, Oid operator,Datum constval, bool constisnull,bool varonleft);
static double var_mk_eq_non_const(VariableStatData *vardata, Oid operator, Node *other, bool varonleft);

static double
mk_eqjoinsel_inner(Oid operator, VariableStatData *vardata1, VariableStatData *vardata2);
static double
mk_eqjoinsel_semi(Oid operator, VariableStatData *vardata1, VariableStatData *vardata2, 
    RelOptInfo *inner_rel);
static RelOptInfo *find_join_input_rel(PlannerInfo *root, Relids relids);

#define DEFAULT_MK_EQ_SEL       0.005
#define DEFAULT_MK_NULL_SEL     0.03
#define DEFAULT_MK_NONNULL_SEL  0.97

#define mk_DirectFunctionCall4(func, arg1, arg2, arg3, arg4) \
        mk_DirectFunctionCall4Coll(func, InvalidOid, arg1, arg2, arg3, arg4)

#define mk_DirectFunctionCall5(func, arg1, arg2, arg3, arg4, arg5) \
        mk_DirectFunctionCall5Coll(func, InvalidOid, arg1, arg2, arg3, arg4, arg5)

/*************************************************
 * Crude null selectivity optimisations
 ************************************************/

PG_FUNCTION_INFO_V1(mknull_sel);

Datum
mknull_sel(PG_FUNCTION_ARGS)
{
    PG_RETURN_FLOAT8((float8) DEFAULT_MK_NULL_SEL);
}

PG_FUNCTION_INFO_V1(mk_not_null_sel);

Datum
mk_not_null_sel(PG_FUNCTION_ARGS)
{
    PG_RETURN_FLOAT8((float8) DEFAULT_MK_NONNULL_SEL);
}


/*************************************************
 * marked null equivalent of restrict clause eqsel
 *************************************************/

PG_FUNCTION_INFO_V1(mk_eqsel);

Datum
mk_eqsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid                     operator = PG_GETARG_OID(1);
    List       *args = (List *) PG_GETARG_POINTER(2);
    int                     varRelid = PG_GETARG_INT32(3);
    VariableStatData vardata;
    Node       *other;
    bool            varonleft;
    double          selec;

    //check if expression of the form (variable op pseudoconstant)
    // or (pseudoconstant op variable)
    // If not just return a default
    if (!get_restriction_variable(root, args, varRelid, 
        &vardata, &other, &varonleft))
        
        PG_RETURN_FLOAT8(DEFAULT_MK_EQ_SEL);


    if (IsA(other, Const))
        selec = var_mk_eq_const(&vardata, operator,
            ((Const *) other)->constvalue,
            ((Const *) other)->constisnull,
            varonleft);
    else
        selec = var_mk_eq_non_const(&vardata, operator, 
            other,
            varonleft);

    ReleaseVariableStats(vardata);

    PG_RETURN_FLOAT8((float8) selec);
}



static double
var_mk_eq_const(VariableStatData *vardata, Oid operator,
                         Datum constval, bool constisnull,
                         bool varonleft)
{
    double      selec;
    bool        isdefault;

    //If the constant is an SQL null assume operator is strict and return zero
    if (constisnull)
        return 0.0;

    if (vardata->isunique && vardata->rel && vardata->rel->tuples >= 1.0)
        return 1.0 / vardata->rel->tuples;

    if (HeapTupleIsValid(vardata->statsTuple))
    {
        Form_pg_statistic stats;
        Datum   *values;
        int     nvalues;
        float4  *numbers;
        int     nnumbers;
        bool    match = false;
        int     i;

        stats = (Form_pg_statistic) GETSTRUCT(vardata->statsTuple);

        if (get_attstatsslot(vardata->statsTuple,
                    vardata->atttype, vardata->atttypmod,
                    STATISTIC_KIND_MCV, InvalidOid,
                    NULL,
                    &values, &nvalues,
                    &numbers, &nnumbers))
        {
            FmgrInfo        eqproc;

            fmgr_info(get_opcode(operator), &eqproc);

            for (i = 0; i < nvalues; i++)
            {
                if (varonleft)
                    {
                    match = DatumGetBool(mk_FunctionCall2Coll(&eqproc,

  DEFAULT_COLLATION_OID,

          values[i],

          constval));
                }
                else
                    match = DatumGetBool(mk_FunctionCall2Coll(&eqproc,

  DEFAULT_COLLATION_OID,

          constval,

          values[i]));
                if (match)
                    break;
            }
        }
        else
        {
            values = NULL;
            numbers = NULL;
            i = nvalues = nnumbers = 0;
        }

        if (match)
        {
            selec = numbers[i];
        }
        else
        {
            double      sumcommon = 0.0;
            double      otherdistinct;

            for (i = 0; i < nnumbers; i++)
                sumcommon += numbers[i];
            selec = 1.0 - sumcommon - stats->stanullfrac;
            CLAMP_PROBABILITY(selec);

            otherdistinct = get_variable_numdistinct(vardata, &isdefault) - nnumbers;
            if (otherdistinct > 1)
                selec /= otherdistinct;


            if (nnumbers > 0 && selec > numbers[nnumbers - 1])
                selec = numbers[nnumbers - 1];
            }

            free_attstatsslot(vardata->atttype, values, nvalues,
                                                  numbers, nnumbers);
    }
    else
    {
        selec = 1.0 / get_variable_numdistinct(vardata, &isdefault);
    }

    CLAMP_PROBABILITY(selec);

    return selec;
}


static double
var_mk_eq_non_const(VariableStatData *vardata, Oid operator,
                                 Node *other,
                                 bool varonleft)
{
    double      selec;
    bool        isdefault;

    if (vardata->isunique && vardata->rel && vardata->rel->tuples >= 1.0)
        return 1.0 / vardata->rel->tuples;

    if (HeapTupleIsValid(vardata->statsTuple))
    {
        Form_pg_statistic stats;
        double          ndistinct;
        float4     *numbers;
        int                     nnumbers;

        stats = (Form_pg_statistic) GETSTRUCT(vardata->statsTuple);

        selec = 1.0 - stats->stanullfrac;
        ndistinct = get_variable_numdistinct(vardata, &isdefault);
        if (ndistinct > 1)
            selec /= ndistinct;

        if (get_attstatsslot(vardata->statsTuple,
                vardata->atttype, vardata->atttypmod,
                STATISTIC_KIND_MCV, InvalidOid,
                NULL,
                NULL, NULL,
                &numbers, &nnumbers))
        {
            if (nnumbers > 0 && selec > numbers[0])
                selec = numbers[0];
            free_attstatsslot(vardata->atttype, NULL, 0, numbers, nnumbers);
        }
    }
    else
    {
        selec = 1.0 / get_variable_numdistinct(vardata, &isdefault);
    }

    CLAMP_PROBABILITY(selec);

    return selec;
}


/**************************************************
 * marked null equivalent of restrict clause neqsel
 *************************************************/

PG_FUNCTION_INFO_V1(mk_neqsel);

Datum
mk_neqsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid                     operator = PG_GETARG_OID(1);
    List       *args = (List *) PG_GETARG_POINTER(2);
    int                     varRelid = PG_GETARG_INT32(3);
    Oid                     eqop;
    float8          result;


    eqop = get_negator(operator);
    if (eqop)
    {
        result = DatumGetFloat8(mk_DirectFunctionCall4(mk_eqsel,
            PointerGetDatum(root),
            ObjectIdGetDatum(eqop),
            PointerGetDatum(args),
            Int32GetDatum(varRelid)));
    }
    else
    {
        /* Use default selectivity (should we raise an error instead?) */
        result = DEFAULT_EQ_SEL;
    }
    result = 1.0 - result;
    fprintf(stderr,"not equal selectivity is %f",result);
    PG_RETURN_FLOAT8(result);
}





/******************************************************************************
 * marked null equivalent of join clause eqjoinsel
 * only difference to eqjoinsel is we call mk_eqjoinsel_inner and
 * mk_eqjoinsel_semi instead of eqjoinsel_inner and eqjoinsel_semi respectively
 *****************************************************************************/

PG_FUNCTION_INFO_V1(mk_eqjoinsel);

Datum
mk_eqjoinsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid                     operator = PG_GETARG_OID(1);
    List       *args = (List *) PG_GETARG_POINTER(2);

#ifdef NOT_USED
    JoinType        jointype = (JoinType) PG_GETARG_INT16(3);
#endif
    SpecialJoinInfo *sjinfo = (SpecialJoinInfo *) PG_GETARG_POINTER(4);
    double          selec;
    VariableStatData vardata1;
    VariableStatData vardata2;
    bool            join_is_reversed;
    RelOptInfo *inner_rel;

    get_join_variables(root, args, sjinfo, &vardata1, &vardata2, &join_is_reversed);

    switch (sjinfo->jointype)
    {
        case JOIN_INNER:
        case JOIN_LEFT:
        case JOIN_FULL:
            selec = mk_eqjoinsel_inner(operator, &vardata1, &vardata2);
            break;
        case JOIN_SEMI:
        case JOIN_ANTI:

            inner_rel = find_join_input_rel(root, sjinfo->min_righthand);

            if (!join_is_reversed)
                selec = mk_eqjoinsel_semi(operator, &vardata1, &vardata2, inner_rel);
            else
                selec = mk_eqjoinsel_semi(get_commutator(operator), &vardata2, &vardata1, inner_rel);
            break;
        default:
            elog(ERROR, "unrecognized join type: %d", (int) sjinfo->jointype);
            selec = 0; 
            break;
    }

    ReleaseVariableStats(vardata1);
    ReleaseVariableStats(vardata2);

    CLAMP_PROBABILITY(selec);

    PG_RETURN_FLOAT8((float8) selec);
}

/*********************************************************************
 * An exact copy of eqjoinsel_inner but with call to FunctionCall2Coll
 * replaced with call to mk_FunctionCall2Coll
 ********************************************************************/

static double
mk_eqjoinsel_inner(Oid operator, VariableStatData *vardata1, VariableStatData *vardata2)
{
        double          selec;
        double          nd1;
        double          nd2;
        bool            isdefault1;
        bool            isdefault2;
        Form_pg_statistic stats1 = NULL;
        Form_pg_statistic stats2 = NULL;
        bool            have_mcvs1 = false;
        Datum      *values1 = NULL;
        int                     nvalues1 = 0;
        float4     *numbers1 = NULL;
        int                     nnumbers1 = 0;
        bool            have_mcvs2 = false;
        Datum      *values2 = NULL;
        int                     nvalues2 = 0;
        float4     *numbers2 = NULL;
        int                     nnumbers2 = 0;

        nd1 = get_variable_numdistinct(vardata1, &isdefault1);
        nd2 = get_variable_numdistinct(vardata2, &isdefault2);

        if (HeapTupleIsValid(vardata1->statsTuple))
        {
                stats1 = (Form_pg_statistic) GETSTRUCT(vardata1->statsTuple);
                have_mcvs1 = get_attstatsslot(vardata1->statsTuple,
                                                                          vardata1->atttype,
                                                                          vardata1->atttypmod,
                                                                          STATISTIC_KIND_MCV,
                                                                          InvalidOid,
                                                                          NULL,
                                                                          &values1, &nvalues1,
                                                                          &numbers1, &nnumbers1);
        }

        if (HeapTupleIsValid(vardata2->statsTuple))
        {
                stats2 = (Form_pg_statistic) GETSTRUCT(vardata2->statsTuple);
                have_mcvs2 = get_attstatsslot(vardata2->statsTuple,
                                                                          vardata2->atttype,
                                                                          vardata2->atttypmod,
                                                                          STATISTIC_KIND_MCV,
                                                                          InvalidOid,
                                                                          NULL,
                                                                          &values2, &nvalues2,
                                                                          &numbers2, &nnumbers2);
        }

        if (have_mcvs1 && have_mcvs2)
        {

                FmgrInfo        eqproc;
                bool       *hasmatch1;
                bool       *hasmatch2;
                double          nullfrac1 = stats1->stanullfrac;
                double          nullfrac2 = stats2->stanullfrac;
                double          matchprodfreq,
                                        matchfreq1,
                                        matchfreq2,
                                        unmatchfreq1,
                                        unmatchfreq2,
                                        otherfreq1,
                                        otherfreq2,
                                        totalsel1,
                                        totalsel2;
                int                     i,
                                        nmatches;

                fmgr_info(get_opcode(operator), &eqproc);
                hasmatch1 = (bool *) palloc0(nvalues1 * sizeof(bool));
                hasmatch2 = (bool *) palloc0(nvalues2 * sizeof(bool));

                matchprodfreq = 0.0;
                nmatches = 0;
                for (i = 0; i < nvalues1; i++)
                {
                        int                     j;

                        for (j = 0; j < nvalues2; j++)
                        {
                                if (hasmatch2[j])
                                        continue;
                                if (DatumGetBool(mk_FunctionCall2Coll(&eqproc,
                                                                                                   DEFAULT_COLLATION_OID,
                                                                                                   values1[i],
                                                                                                   values2[j])))
                                {
                                        hasmatch1[i] = hasmatch2[j] = true;
                                        matchprodfreq += numbers1[i] * numbers2[j];
                                        nmatches++;
                                        break;
                                }
                        }
                }
                CLAMP_PROBABILITY(matchprodfreq);
                /* Sum up frequencies of matched and unmatched MCVs */
                matchfreq1 = unmatchfreq1 = 0.0;
                for (i = 0; i < nvalues1; i++)
                {
                        if (hasmatch1[i])
                                matchfreq1 += numbers1[i];
                        else
                                unmatchfreq1 += numbers1[i];
                }
                CLAMP_PROBABILITY(matchfreq1);
                CLAMP_PROBABILITY(unmatchfreq1);
                matchfreq2 = unmatchfreq2 = 0.0;
                for (i = 0; i < nvalues2; i++)
                {
                        if (hasmatch2[i])
                                matchfreq2 += numbers2[i];
                        else
                                unmatchfreq2 += numbers2[i];
                }
                CLAMP_PROBABILITY(matchfreq2);
                CLAMP_PROBABILITY(unmatchfreq2);
                pfree(hasmatch1);
                pfree(hasmatch2);


                otherfreq1 = 1.0 - nullfrac1 - matchfreq1 - unmatchfreq1;
                otherfreq2 = 1.0 - nullfrac2 - matchfreq2 - unmatchfreq2;
                CLAMP_PROBABILITY(otherfreq1);
                CLAMP_PROBABILITY(otherfreq2);


                totalsel1 = matchprodfreq;
                if (nd2 > nvalues2)
                        totalsel1 += unmatchfreq1 * otherfreq2 / (nd2 - nvalues2);
                if (nd2 > nmatches)
                        totalsel1 += otherfreq1 * (otherfreq2 + unmatchfreq2) /
                                (nd2 - nmatches);
                /* Same estimate from the point of view of relation 2. */
                totalsel2 = matchprodfreq;
                if (nd1 > nvalues1)
                        totalsel2 += unmatchfreq2 * otherfreq1 / (nd1 - nvalues1);
                if (nd1 > nmatches)
                        totalsel2 += otherfreq2 * (otherfreq1 + unmatchfreq1) /
                                (nd1 - nmatches);

                selec = (totalsel1 < totalsel2) ? totalsel1 : totalsel2;
        }
        else
        {

                double          nullfrac1 = stats1 ? stats1->stanullfrac : 0.0;
                double          nullfrac2 = stats2 ? stats2->stanullfrac : 0.0;

                selec = (1.0 - nullfrac1) * (1.0 - nullfrac2);
                if (nd1 > nd2)
                        selec /= nd1;
                else
                        selec /= nd2;
        }

        if (have_mcvs1)
                free_attstatsslot(vardata1->atttype, values1, nvalues1,
                                                  numbers1, nnumbers1);
        if (have_mcvs2)
                free_attstatsslot(vardata2->atttype, values2, nvalues2,
                                                  numbers2, nnumbers2);

        return selec;
}


/*********************************************************************
 * An exact copy of eqjoinsel_semi but with call to FunctionCall2Coll
 * replaced with call to mk_FunctionCall2Coll
 *********************************************************************/


static double
mk_eqjoinsel_semi(Oid operator, VariableStatData *vardata1, VariableStatData *vardata2, 
    RelOptInfo *inner_rel)
{
        double          selec;
        double          nd1;
        double          nd2;
        bool            isdefault1;
        bool            isdefault2;
        Form_pg_statistic stats1 = NULL;
        bool            have_mcvs1 = false;
        Datum      *values1 = NULL;
        int                     nvalues1 = 0;
        float4     *numbers1 = NULL;
        int                     nnumbers1 = 0;
        bool            have_mcvs2 = false;
        Datum      *values2 = NULL;
        int                     nvalues2 = 0;
        float4     *numbers2 = NULL;
        int                     nnumbers2 = 0;

        nd1 = get_variable_numdistinct(vardata1, &isdefault1);
        nd2 = get_variable_numdistinct(vardata2, &isdefault2);

        if (vardata2->rel)
                nd2 = Min(nd2, vardata2->rel->rows);
        nd2 = Min(nd2, inner_rel->rows);

        if (HeapTupleIsValid(vardata1->statsTuple))
        {
                stats1 = (Form_pg_statistic) GETSTRUCT(vardata1->statsTuple);
                have_mcvs1 = get_attstatsslot(vardata1->statsTuple,
                                                                          vardata1->atttype,
                                                                          vardata1->atttypmod,
                                                                          STATISTIC_KIND_MCV,
                                                                          InvalidOid,
                                                                          NULL,
                                                                          &values1, &nvalues1,
                                                                          &numbers1, &nnumbers1);
        }

        if (HeapTupleIsValid(vardata2->statsTuple))
        {
                have_mcvs2 = get_attstatsslot(vardata2->statsTuple,
                                                                          vardata2->atttype,
                                                                          vardata2->atttypmod,
                                                                          STATISTIC_KIND_MCV,
                                                                          InvalidOid,
                                                                          NULL,
                                                                          &values2, &nvalues2,
                                                                          &numbers2, &nnumbers2);
        }

        if (have_mcvs1 && have_mcvs2 && OidIsValid(operator))
        {

                FmgrInfo        eqproc;
                bool       *hasmatch1;
                bool       *hasmatch2;
                double          nullfrac1 = stats1->stanullfrac;
                double          matchfreq1,
                                        uncertainfrac,
                                        uncertain;
                int                     i,
                                        nmatches,
                                        clamped_nvalues2;

                clamped_nvalues2 = Min(nvalues2, nd2);

                fmgr_info(get_opcode(operator), &eqproc);
                hasmatch1 = (bool *) palloc0(nvalues1 * sizeof(bool));
                hasmatch2 = (bool *) palloc0(clamped_nvalues2 * sizeof(bool));

                nmatches = 0;
                for (i = 0; i < nvalues1; i++)
                {
                        int                     j;

                        for (j = 0; j < clamped_nvalues2; j++)
                        {
                                if (hasmatch2[j])
                                        continue;
                                if (DatumGetBool(mk_FunctionCall2Coll(&eqproc,
                                                                                                   DEFAULT_COLLATION_OID,
                                                                                                   values1[i],
                                                                                                   values2[j])))
                                {
                                        hasmatch1[i] = hasmatch2[j] = true;
                                        nmatches++;
                                        break;
                                }
                        }
                }
                matchfreq1 = 0.0;
                for (i = 0; i < nvalues1; i++)
                {
                        if (hasmatch1[i])
                                matchfreq1 += numbers1[i];
                }
                CLAMP_PROBABILITY(matchfreq1);
                pfree(hasmatch1);
                pfree(hasmatch2);

                if (!isdefault1 && !isdefault2)
                {
                        nd1 -= nmatches;
                        nd2 -= nmatches;
                        if (nd1 <= nd2 || nd2 < 0)
                                uncertainfrac = 1.0;
                        else
                                uncertainfrac = nd2 / nd1;
                }
                else
                        uncertainfrac = 0.5;
                uncertain = 1.0 - matchfreq1 - nullfrac1;
                CLAMP_PROBABILITY(uncertain);
                selec = matchfreq1 + uncertainfrac * uncertain;
        }
        else
        {

                double          nullfrac1 = stats1 ? stats1->stanullfrac : 0.0;

                if (!isdefault1 && !isdefault2)
                {
                        if (nd1 <= nd2 || nd2 < 0)
                                selec = 1.0 - nullfrac1;
                        else
                                selec = (nd2 / nd1) * (1.0 - nullfrac1);
                }
                else
                        selec = 0.5 * (1.0 - nullfrac1);
        }

        if (have_mcvs1)
                free_attstatsslot(vardata1->atttype, values1, nvalues1,
                                                  numbers1, nnumbers1);
        if (have_mcvs2)
                free_attstatsslot(vardata2->atttype, values2, nvalues2,
                                                  numbers2, nnumbers2);

        return selec;
}





static RelOptInfo *
find_join_input_rel(PlannerInfo *root, Relids relids)
{
        RelOptInfo *rel = NULL;

        switch (bms_membership(relids))
        {
                case BMS_EMPTY_SET:
                        /* should not happen */
                        break;
                case BMS_SINGLETON:
                        rel = find_base_rel(root, bms_singleton_member(relids));
                        break;
                case BMS_MULTIPLE:
                        rel = find_join_rel(root, relids);
                        break;
        }

        if (rel == NULL)
                elog(ERROR, "could not find RelOptInfo for given relids");

        return rel;
}

/**********************************************************************************
 * marked null equivalent of join clause neqjoinsel
 * only difference to neqjoinsel is we call mk_DirectFunctionCall5 and mk_eqjoinsel
 * instead of DirectFunctionCall5 and eqjoinsel respectively
 *********************************************************************************/

PG_FUNCTION_INFO_V1(mk_neqjoinsel);

Datum
mk_neqjoinsel(PG_FUNCTION_ARGS)
{
    PlannerInfo *root = (PlannerInfo *) PG_GETARG_POINTER(0);
    Oid                     operator = PG_GETARG_OID(1);
    List       *args = (List *) PG_GETARG_POINTER(2);
    JoinType        jointype = (JoinType) PG_GETARG_INT16(3);
    SpecialJoinInfo *sjinfo = (SpecialJoinInfo *) PG_GETARG_POINTER(4);
    Oid                     eqop;
    float8          result;

    eqop = get_negator(operator);
    if (eqop)
    {
        result = DatumGetFloat8(mk_DirectFunctionCall5(mk_eqjoinsel,
            PointerGetDatum(root),
            ObjectIdGetDatum(eqop),
            PointerGetDatum(args),
            Int16GetDatum(jointype),
            PointerGetDatum(sjinfo)));
    }
    else
    {
        /* Use default selectivity (should we raise an error instead?) */
        result = DEFAULT_EQ_SEL;
    }
    result = 1.0 - result;
    PG_RETURN_FLOAT8(result);
}



/*****************************************************************
 * variant of FunctionCall2Coll which doesn't break on null output
 ****************************************************************/

static Datum
mk_FunctionCall2Coll(FmgrInfo *flinfo, Oid collation, Datum arg1, Datum arg2)
{
    FunctionCallInfoData fcinfo;
    Datum           result;

    InitFunctionCallInfoData(fcinfo, flinfo, 2, collation, NULL, NULL);

    fcinfo.arg[0] = arg1;
    fcinfo.arg[1] = arg2;
    fcinfo.argnull[0] = false;
    fcinfo.argnull[1] = false;

    result = FunctionCallInvoke(&fcinfo);

    /* differ from original function FunctionCall2Coll in that we just treat null results as false */
    if (fcinfo.isnull)
    {
        //fprintf(stderr,"FunctionCall2Coll returns null would have previously failed here\n");
        result = BoolGetDatum(false);
    }

    return result;
}


static Datum
mk_DirectFunctionCall4Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
                                                Datum arg3, Datum arg4)
{
        FunctionCallInfoData fcinfo;
        Datum           result;

        InitFunctionCallInfoData(fcinfo, NULL, 4, collation, NULL, NULL);

        fcinfo.arg[0] = arg1;
        fcinfo.arg[1] = arg2;
        fcinfo.arg[2] = arg3;
        fcinfo.arg[3] = arg4;
        fcinfo.argnull[0] = false;
        fcinfo.argnull[1] = false;
        fcinfo.argnull[2] = false;
        fcinfo.argnull[3] = false;

        result = (*func) (&fcinfo);

        /* differ from original function DirectFunctionCall4Coll in that we just treat null results as false */
        if (fcinfo.isnull)
        {
            //fprintf(stderr,"DirectFunctionCall4Coll returns null would have previously failed here\n");
            //test whether this should be true or false!
            result = BoolGetDatum(false);
        }
        return result;
}


static Datum
mk_DirectFunctionCall5Coll(PGFunction func, Oid collation, Datum arg1, Datum arg2,
                                                Datum arg3, Datum arg4, Datum arg5)
{
        FunctionCallInfoData fcinfo;
        Datum           result;

        InitFunctionCallInfoData(fcinfo, NULL, 5, collation, NULL, NULL);

        fcinfo.arg[0] = arg1;
        fcinfo.arg[1] = arg2;
        fcinfo.arg[2] = arg3;
        fcinfo.arg[3] = arg4;
        fcinfo.arg[4] = arg5;
        fcinfo.argnull[0] = false;
        fcinfo.argnull[1] = false;
        fcinfo.argnull[2] = false;
        fcinfo.argnull[3] = false;
        fcinfo.argnull[4] = false;

        result = (*func) (&fcinfo);

        /* differ from original function DirectFunctionCall5Coll in that we just treat null results as false */
        if (fcinfo.isnull)
        {
            result = BoolGetDatum(false);
        }
        return result;
}

